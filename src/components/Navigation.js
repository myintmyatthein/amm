import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserGraduate, faHome } from '@fortawesome/free-solid-svg-icons'

import { BrowserRouter as Router, Route, Link } from "react-router-dom";


import StudentList from './students/StudentList'
import Home from './Home'
import StudentDetail from './students/StudentDetail'
import StudentAdmission from './students/StudentAdmission'

// Colors
import teal from '@material-ui/core/colors/teal'

const primary = teal[800];

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: primary
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  title: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  nested: {
    paddingLeft: theme.spacing(6),
  },
  link: {
    textDecoration: "none",
    color: "#000"
  }
}));

function Index() {
  return <Home/>
}

function studentList() {
  return <StudentList/>
}

function studentDetail() {
  return <StudentDetail/>
}

function studentAdmission() {
  return <StudentAdmission/>
}

function Navigation() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  const [stuopen, setStuOpen] = React.useState(false);

  function handleClick() {
    setStuOpen(!stuopen);
  }

  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />

        {/* App Bar */}
          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: open,
            })}
          >
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap className={classes.title}>
                AMM Education Center
              </Typography>
              <Button color="inherit" className={classes.button}>Login</Button>
            </Toolbar>
          </AppBar>
        {/* App Bar Ends */}

        {/* Navigation Section */}
          <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
                <MenuIcon/>
              </IconButton>
              <Typography variant="h3">AMM</Typography>
            </div>
            <Divider />

            {/* Navigation List */}
              <List>

                {/* Home */}
                  <Link to="/" className={classes.link}>
                    <ListItem button>
                      <ListItemIcon>
                        <FontAwesomeIcon icon={faHome} style={{fontSize:`20px`}}/>
                      </ListItemIcon>
                      <ListItemText primary="Home" />
                    </ListItem>
                  </Link>
                {/* Home Ends */}

                {/* Student */}
                <ListItem button onClick={handleClick}>
                  <ListItemIcon>
                  <FontAwesomeIcon icon={faUserGraduate} style={{fontSize:`20px`}}/>
                  </ListItemIcon>
                  <ListItemText primary="Student" />
                  {stuopen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={stuopen} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <Link to="/studentlist" className={classes.link}>
                      <ListItem button className={classes.nested}>
                        <ListItemText primary="Student List" />
                      </ListItem>
                    </Link>
                    <Link to="/studentDetail" className={classes.link}>
                      <ListItem button className={classes.nested}>
                        <ListItemText primary="Student Detail" />
                      </ListItem>
                    </Link>
                    <Link to="/studentAdmission" className={classes.link}>
                      <ListItem button className={classes.nested}>
                        <ListItemText primary="Student Admission" />
                      </ListItem>
                    </Link>
                  </List>
                </Collapse>
                {/* Student End */}

              </List>
            {/* Navigation List Ends */}

          </Drawer>
        {/* Navigation Section Ends */}

        {/* Content Section */}
          <main
            className={clsx(classes.content, {
              [classes.contentShift]: open,
            })}
          >
            <div className={classes.drawerHeader} />
            
            <Route path="/" exact component={Index} />
            <Route path="/studentList" exact component={studentList} />
            <Route path="/studentDetail" exact component={studentDetail} />
            <Route path="/studentAdmission" exact component={studentAdmission} />

          </main>
        {/* Content Section Ends */}
      </div>
    </Router>
  );
}

export default Navigation;