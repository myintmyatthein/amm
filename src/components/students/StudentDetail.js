import React, {Component} from 'react'
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight, faGraduationCap, faEnvelope } from '@fortawesome/free-solid-svg-icons'

import { Grid, Button, Divider } from '@material-ui/core';

class StudentDetail extends Component {
    render() {
        return (
            <div>
                <Paper elevation={0} >
                    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="Breadcrumb">
                    <Link color="inherit" href="/">
                        Home
                    </Link>
                    <Link color="inherit" href="/getting-started/installation/">
                        Student
                    </Link>
                    <Typography color="textPrimary">Student Detail</Typography>
                    </Breadcrumbs>
                </Paper>

                <Paper style={{marginTop: 20}}>
                    <Grid container spacing={2} style={{display:'flex', alignItems:'center'}}>
                        <Grid item md={4}>
                            <h3 style={{marginLeft: 10}}>Myint Myat Thein</h3>
                        </Grid>
                        <Grid item md={5} style={{padding:`15px 0`}}>
                            <div style={{display:'flex', flexDirection:'row', justifyContent:'flex-end', borderRight:`1px solid #000`, paddingRight:40, borderRightStyle:'dotted'}}>
                                <div style={{ border:`1px solid #000`, padding: `10px 15px`, borderRadius:`50%`}}>
                                    <FontAwesomeIcon icon={faAngleLeft} style={{fontSize:`20px`}}/>
                                </div>
                                <div style={{ border:`1px solid #000`, padding: `10px 15px`, borderRadius:`50%`, marginLeft: 10}}>
                                    <FontAwesomeIcon icon={faAngleRight} style={{fontSize:`20px`}}/>
                                </div>
                            </div>
                        </Grid>
                        <Grid item md={3}>
                            <div style={{display:'flex', justifyContent:'center'}}>
                            <Button style={{border:"none", backgroundColor:`rgba(0, 0, 255, 0.4)`, padding: `8px 15px`, borderRadius: 30, fontSize: 10}}>Edit Profile</Button>
                            <Button style={{border:"none", backgroundColor:`rgba(255, 0, 0, 0.6)`, padding: `8px 15px`, borderRadius: 30, fontSize: 10, marginLeft: 10}}>Delete Profile</Button>
                            </div>
                        </Grid>
                    </Grid>
                    <Divider/>
                    <Grid container spacing={2}>
                        <Grid item md={2} style={{padding:35,}}>
                            <img src="https://microhealth.com/assets/images/illustrations/personal-user-illustration-@2x.png" alt="student" width="90" height="100" style={{backgroundColor:'#f4f4f4',borderRadius: 20, padding: 10}}/>
                        </Grid>
                        <Grid item md={10} style={{marginTop:10, paddingRight:40}}>
                                <Grid container style={{backgroundColor:'#f4f4f4'}}>
                                    <Grid md={5} style={{padding:`9px 0`}}>
                                        <div style={{display:'flex', flexDirection:'column'}}>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 40, fontWeight:'bold'}}>Status</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p">Enroll</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 40, fontWeight:'bold'}}>Arrival Date</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p">01 June 2019</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 40, fontWeight:'bold'}}>Departure</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p">30 October 2019</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 40, fontWeight:'bold'}}>Progress</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p">18/24 Weeks</Typography>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    </Grid>
                                    <hr style={{height:80, margin:`10px 40px`, opacity:0.4}}/>
                                    <Grid md={5} style={{padding:`9px 0`}}>
                                        <div style={{display:'flex', flexDirection:'column',}}>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 0, fontWeight:'bold'}}>Course</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p">Mathematics</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 0, fontWeight:'bold'}}>Actual Attendance</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{textAlign:'center'}}>90%</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={2}>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p" style={{marginLeft: 0, fontWeight:'bold'}}>Transportation</Typography>
                                                </Grid>
                                                <Grid item md={6} style={{padding:15}}>
                                                    <Typography variant="p">Yes</Typography>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    </Grid>
                                </Grid>
                        </Grid>
                    </Grid>
                </Paper>

                <Grid container spacing={2} style={{marginTop:10}}>
                    <Grid item md={6}>
                        {/* Student Detail */}
                        <Paper>
                            <div style={{padding:10}}>
                                <Typography variant="h6" style={{marginLeft:20}}>
                                    <div style={{display:'flex', flexDirection:'row', alignItems:'center'}}>
                                        <div style={{padding:`5px 10px`, backgroundColor:'#f4f4f4', borderRadius:`50%`}}>
                                            <FontAwesomeIcon icon={faGraduationCap} style={{backgroundColor:'#f4f4f4', borderRadius:`50%`}}/>
                                        </div>
                                        <div style={{marginLeft: 20}}>
                                            Student Detail
                                        </div>
                                    </div>
                                    
                                </Typography>
                            </div>
                            <Divider/>
                            <Grid container spacing={2}>
                                <Grid item md={6} style={{display:'flex',flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                                    <Typography variant="p" style={{margin:`20px 60px 10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>Student Name</Typography>
                                    <Typography variant="p" style={{margin:`10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>Date of Birth</Typography>
                                    <Typography variant="p" style={{margin:`10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>Gender</Typography>
                                </Grid>
                                <Grid item md={6} style={{display:'flex',flexDirection:'column', alignItems:'center'}}>
                                    <Typography variant="p" style={{margin:`20px 10px 10px 10px`, alignSelf:'flex-start'}}>Myint Myat Thein</Typography>
                                    <Typography variant="p" style={{margin:10, alignSelf:'flex-start'}}>4/5/1997</Typography>
                                    <Typography variant="p" style={{margin:10, alignSelf:'flex-start'}}>Male</Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item md={6}>
                        {/* Student Contact */}
                        <Paper>
                            <div style={{padding:10}}>
                                <Typography variant="h6" style={{marginLeft:20}}>
                                    <div style={{display:'flex', flexDirection:'row', alignItems:'center'}}>
                                        <div style={{padding:`5px 10px`, backgroundColor:'#f4f4f4', borderRadius:`50%`}}>
                                            <FontAwesomeIcon icon={faEnvelope} style={{backgroundColor:'#f4f4f4', borderRadius:`50%`}}/>
                                        </div>
                                        <div style={{marginLeft: 20}}>
                                            Contact Information
                                        </div>
                                    </div>
                                    
                                </Typography>
                            </div>
                            <Divider/>
                            <Grid container spacing={2}>
                                <Grid item md={6} style={{display:'flex',flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                                    <Typography variant="p" style={{margin:`10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>Home Address</Typography>
                                    <Typography variant="p" style={{margin:`10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>State</Typography>
                                    <Typography variant="p" style={{margin:`10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>Email</Typography>
                                    <Typography variant="p" style={{margin:`10px 60px`, alignSelf:'flex-start', fontWeight:'bold'}}>Phone</Typography>
                                </Grid>
                                <Grid item md={6} style={{display:'flex',flexDirection:'column', alignItems:'center'}}>
                                    <Typography variant="p" style={{margin:10, alignSelf:'flex-start'}}>15st and 16st</Typography>
                                    <Typography variant="p" style={{margin:10, alignSelf:'flex-start'}}>Mandalay</Typography>
                                    <Typography variant="p" style={{margin:10, alignSelf:'flex-start'}}>myintmyatthein@gmail.com</Typography>
                                    <Typography variant="p" style={{margin:10, alignSelf:'flex-start'}}>09-402728583</Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default StudentDetail;