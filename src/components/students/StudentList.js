import React from 'react';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import Grid from '@material-ui/core/Grid';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import {Form, Col} from 'react-bootstrap'

function createData(sino, admissionno, name, admissiondate, course, batch) {
  return { sino, admissionno, name, admissiondate, course, batch };
}

const rows = [
  createData(1, "AMM-01", "Mg Ba", "3/6/2019", 'Maths', 'B'),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

class StudentList extends React.Component {
  render() {
  return (
    <div>

      {/* Breadcrumbs */}
      <Paper elevation={0} style={{padding:`1%`}}>
        <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="Breadcrumb">
          <Link color="inherit" href="/">
            Home
          </Link>
          <Link color="inherit" href="/getting-started/installation/">
            Student
          </Link>
          <Typography color="textPrimary">Student List</Typography>
        </Breadcrumbs>
      </Paper>


      {/* Search Section */}
      <Grid container spacing={2} style={{marginTop: 15}}>
          <Grid item md={3} style={{textAlign:"center"}}>
              <Form>
                <Form.Group as={Col} controlId="formGridState">
                  <Form.Control as="select" style={{width: `80%`, padding:`5px 0`, float: 'left'}}>
                    <option>Choose Course</option>
                    <option>...</option>
                  </Form.Control>
                </Form.Group>
              </Form>
          </Grid>
          <Grid item md={3}>
            <Form>
              <Form.Group as={Col} controlId="formGridState">
                <Form.Control as="select" style={{width: `80%`, padding: 5,}}>
                  <option>Choose Course</option>
                  <option>...</option>
                </Form.Control>
              </Form.Group>
            </Form>
          </Grid>
          <Grid item md={6}>
          <Form>
              <Form.Group as={Col} controlId="formPlaintextEmail"> 
                <Col sm="10">
                  <Form.Control plaintext placeholder="&#128269; Search" style={{width: `50%`, padding: 5, float:"right",}}/>
                </Col>
              </Form.Group>
            </Form>
          </Grid>

            {/* Table Section */}
            <Paper  style={{width:`100%`, marginTop: 13, maxHeight: `400px`, overflow: 'scroll'}}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>SI No</TableCell>
                    <TableCell align="left">Admission No</TableCell>
                    <TableCell align="left">Student Name</TableCell>
                    <TableCell align="left">Admission Date</TableCell>
                    <TableCell align="left">Course</TableCell>
                    <TableCell align="left">Batch</TableCell>
                    <TableCell align="left">Manage</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map(row => (
                    <TableRow key={row.sino}>
                      <TableCell component="th" scope="row">
                        {row.sino}
                      </TableCell>
                      <TableCell align="left">{row.admissionno}</TableCell>
                      <TableCell align="left">{row.name}</TableCell>
                      <TableCell align="left">{row.admissiondate}</TableCell>
                      <TableCell align="left">{row.course}</TableCell>
                      <TableCell align="left">{row.batch}</TableCell>
                      <TableCell align="left">{row.batch}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
      </Grid>
    </div>
  );
                  }
}

export default StudentList;