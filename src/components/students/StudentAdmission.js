import React, {Component} from 'react'

import {Grid, Paper, Breadcrumbs, Link, Typography, TextField} from '@material-ui/core'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'



class StudentAddmission extends Component {
    render() {
        return (
            <div>
                <Paper elevation={0} style={{background:'transparent'}}>
                    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="Breadcrumb">
                    <Link color="inherit" href="/">
                        Home
                    </Link>
                    <Link color="inherit" href="/getting-started/installation/">
                        Student
                    </Link>
                    <Typography color="textPrimary">Student Admission</Typography>
                    </Breadcrumbs>
                </Paper>

                <Grid container spacing={2} style={{marginTop:20}}>
                    <Grid item md={2}>
                        1
                    </Grid>
                    <Grid item md={10}>
                        <Grid container spacing={2}>
                            <Grid item md={12}>
                                <table style={{width:`100%`}}>
                                    <tr>
                                        <td>Student Name:</td>
                                        <td>
                                            <div style={{borderBottom:`1px solid #000`, width:`40%`,padding:7}}>
                                                <input type="text" placeholder="Enter Student Name" style={{background:'transparent', border:'none'}}/>
                                            </div>
                                        </td>
                                        <td>Date of Birth:</td>
                                        <td>
                                        <TextField
                                            id="date"
                                            type="date"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                        </td>
                                    </tr>
                                </table>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default StudentAddmission