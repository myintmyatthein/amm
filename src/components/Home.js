import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserGraduate, faUser, faBook, faBirthdayCake, faHistory, faMoneyBillAlt } from '@fortawesome/free-solid-svg-icons'
import { Typography, Divider } from '@material-ui/core';

import Chart from "react-apexcharts";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(4),
    color: theme.palette.text.secondary,
  },

  number: {
    fontFamily: "Barlow",
    fontWeight: 500,
    color: "#fff",
  },

  // Center-Info
  student: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },

  infoText: {
    fontFamily: "Caramaran",
    textAlign: "center",
    fontSize: 20,
  },

  icon: {
    color: "white",
    fontSize: 60,
    marginRight: `15%`,
  },

  attendance: {
    display: 'flex',
    padding: 0,
    justifyContent: "center",
    alignItems: "center"
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();
  const list = {
    options: {
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
      }
    },
    series: [
      {
        name: "Students",
        data: [30, 40, 45, 50, 49, 60, 70, 91]
      },{
        name: "Employees",
        data: [15, 15, 15, 15, 15, 15, 15, 15]
      }
    ]
  }
  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid container spacing={2}>

          {/* Attendance Graph */}
          <Grid item md={8} >
                <Paper className={classes.paper && classes.attendance}>
                <div style={{display: 'flex', flexDirection:'column'}}>
                  <div style={{padding:8}}>
                          <Typography variant="h6" >Daily Attendance Overview</Typography>
                  </div>
                  <Chart
                    options={list.options}
                    series={list.series}
                    type="bar"
                    width="500"
                  />
                </div>
                </Paper>
            </Grid>
          {/* Attendance Graph Ends */}

          <Grid item md={4}>

            {/* New Feed */}
              <Grid item md={12}>
                  <Paper className={classes.paper} style={{padding:0}}>
                    <div style={{padding:10, backgroundColor:"#4dd0e1"}}>
                          <Typography variant="h6" style={{textAlign:"center"}}>New Feed</Typography>
                    </div>
                  </Paper>
              </Grid>
            {/* New Feed Ends */}

            {/* Birthday */}
              <Grid item md={12} style={{marginTop: 130}}>
                    <Paper className={classes.paper} style={{padding:0}}>
                      <div style={{padding:10, backgroundColor:"#4dd0e1"}}>
                        <Typography variant="h6" style={{textAlign:"center"}}>Birthday</Typography>
                      </div>
                      <Divider/>
                      <div style={{display: "flex", flexDirection: "row", justifyContent:"center", alignItems: "center"}}>
                        <div style={{display:"flex", flexDirection:"column", justifyContent:"center", margin:`5% 0`}}>
                          <Typography variant="h4" style={{alignSelf:"center"}}>2</Typography>
                          <Typography variant="h5">Students</Typography>
                        </div>
                        <FontAwesomeIcon icon={faBirthdayCake} style={{fontSize: 50, margin: `5% 8%`}}/>
                        <div style={{display:"flex", flexDirection:"column", justifyContent:"center", margin:`5% 0`}}>
                          <Typography variant="h4" style={{alignSelf:"center"}}>0</Typography>
                          <Typography variant="h5">Employees</Typography>
                        </div>
                      </div>

                    </Paper>
                </Grid>
              {/* Birthday Ends */}

          </Grid>
          

        </Grid>

        <Grid container spacing={2} style={{marginTop:10}}>

          
          {/* Center Info */}
          <Grid item md={8}>

            <Grid container spacing={2}>

              {/* Student Info */}
                <Grid item md={4} >
                    <Paper className={classes.paper} style={{padding:20, backgroundColor:"#d64933"}}>
                      <div className={classes.student}>
                        <FontAwesomeIcon icon={faUserGraduate} className={classes.icon}/>
                        <Typography variant="h3" className={classes.number}>404</Typography>
                      </div>
                      
                      <Typography variant="h6" className={classes.infoText}>Total Students</Typography>
                    </Paper>
                </Grid>
              {/* Student Info Ends */}

              {/* Employee Info */}
                <Grid item md={4} >
                    <Paper className={classes.paper} style={{padding:20, backgroundColor:"#4592af"}}>
                      <div className={classes.student}>
                        <FontAwesomeIcon icon={faUser} className={classes.icon}/>
                        <Typography variant="h3" className={classes.number}>15</Typography>
                      </div>
                      
                      <Typography variant="h6" className={classes.infoText}>Total Employees</Typography>
                    </Paper>
                </Grid>
              {/* Employee Info Ends */}
              
              {/* Course Info */}
                <Grid item md={4} >
                    <Paper className={classes.paper} style={{padding:20, backgroundColor:"#6ba1a7"}}>
                      <div className={classes.student}>
                        <FontAwesomeIcon icon={faBook} className={classes.icon}/>
                        <Typography variant="h3" className={classes.number}>10</Typography>
                      </div>
                      
                      <Typography variant="h6" className={classes.infoText}>Total Courses</Typography>
                    </Paper>
                </Grid>
              {/* Course Info Ends */}

            </Grid>

            </Grid>
          {/* Center Info Ends */}

          <Grid item md={4} >

            {/* Fee */}
              <Grid item md={12} style={{marginTop:-15}}>
                  <Paper className={classes.paper} style={{padding:0}}>
                    <div style={{padding:10, backgroundColor:"#4dd0e1", display:'flex', flexDirection:'row', alignItems:'center'}}>
                      <Grid item md={6}>
                        <Typography variant="p" style={{textAlign:"left"}}>Fee Collection of the day</Typography>
                      </Grid>
                      <Grid item md={6}>
                        <Typography variant="h6" style={{textAlign:"right"}}> 
                          <FontAwesomeIcon icon={faHistory}/> 6/6/2019</Typography>  
                      </Grid>  
                        
                    </div>
                    <Divider/>
                    <Grid item md={12} style={{display:'flex', flexDirection:'row'}}>

                      <Grid item md={4} >
                        <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', padding: 20}}>
                          <Typography variant="h6">
                            <FontAwesomeIcon icon={faMoneyBillAlt}/> &nbsp; 0 
                          </Typography>
                          <Typography variant="h6">
                            Amount 
                          </Typography>
                        </div>
                      </Grid>

                      <Grid item md={4}>
                        <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', padding: 20}}>
                          <Typography variant="h6">
                            <FontAwesomeIcon icon={faMoneyBillAlt}/> &nbsp; 0 
                          </Typography>
                          <Typography variant="h6">
                            Discount 
                          </Typography>
                        </div>
                      </Grid>

                      <Grid item md={4}>
                        <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', padding: 20}}>
                          <Typography variant="h6">
                            <FontAwesomeIcon icon={faMoneyBillAlt}/> &nbsp; 0 
                          </Typography>
                          <Typography variant="h6">
                            Fine 
                          </Typography>
                        </div> 
                      </Grid>

                    </Grid>
                  </Paper>
              </Grid>
            {/* Fee Ends */}

          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}